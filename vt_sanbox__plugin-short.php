<?php
/*

 * Plugin Name: VT Plugin 1 Shortcode
 * Description: This is a plugin that inserts content using shortcodes
 * Version: 1.0.0
 * Author: Vania Tejuco
 * Author URI: https://www.vaniatejuco.com
 * Text Domain: vt_sandbox__plugin--1
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt

 */

namespace vt_sandbox\short;

// stop unwanted visitors calling directly

if (!defined('ABSPATH')) {
	exit('Go Away!!');
}

/* Displays the shortcode
 * @return Shortcode content
 */

function vt_shortcode1() {
	ob_start();
	?>
<p>Hello, my first shortcode</p>
<?php
return ob_get_clean();
}

add_shortcode('short1', __NAMESPACE__ . '\vt_shortcode1');
